import Vue from 'vue'
import Router from 'vue-router'
// import Home from './views/Home'
// import About from './views/About'
import Team from './views/Team'
import Contestants from './views/Contestants'
import Contests from './views/Contests'
import Message from './views/Message'
import MaterialSecurity from './views/MaterialSecurity'

Vue.use(Router);

export default new Router({
    routes: [
        {
            path: '/',
            name: 'team',
            component: Team
        },
        {
            path: '/contestants',
            name: 'contestants',
            component: Contestants
        },
        {
            path: '/contests',
            name: 'contests',
            component: Contests
        },
        {
            path: '/message',
            name: 'message',
            component: Message
        },
        {
            path: '/material-security',
            name: 'material-security',
            component: MaterialSecurity,
        }
    ]
})
